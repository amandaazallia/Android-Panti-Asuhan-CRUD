package com.example.amanda.panti_asuhan;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.LauncherActivity;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.amanda.panti_asuhan.domain.Asuh;

import java.util.List;
import java.util.ListIterator;

//public class MainActivity extends Activity {
//
//    Siswa siswa = new Siswa();
//    DBAdapter dbAdapter = null;
//
//    EditText txtNama, txtKelas;
//    ListView listSiswa;
//    Button btnSimpan;
//    Siswa editSiswa;
//
//    private static final String OPTION[] = {"Edit", "Delete"};
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//
//        dbAdapter = new DBAdapter(getApplicationContext());
//
//        btnSimpan = (Button) findViewById(R.id.btnSimpan);
//        txtNama = (EditText) findViewById(R.id.txtNama);
//        txtKelas = (EditText) findViewById(R.id.txtKelas);
//        listSiswa = (ListView) findViewById(R.id.listSiswa);
//
//        listSiswa.setOnItemClickListener(new ListItemClick());
//        listSiswa.setAdapter(new ListSiswaAdapter(dbAdapter
//                .getAllSiswa()));
//    }
//

public class MainActivity extends Activity {

    Asuh asuh = new Asuh();
    DBAdapter dbAdapter = null;

    EditText editText, editText2, editText3;
    ListView listAsuh;
    Button button3;
    Asuh editAsuh;

    private  static final String OPTION[] = {"Edit", "Delete"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.anak_asuh);
        dbAdapter = new DBAdapter (getApplicationContext());

        button3 = (Button) findViewById(R.id.button3);
        editText = (EditText) findViewById(R.id.editText);
        editText2 = (EditText) findViewById(R.id.editText2);
        editText3 = (EditText) findViewById(R.id.editText3);
        listAsuh = (ListView) findViewById(R.id.listAsuh);

        listAsuh.setOnItemClickListener(new ListItemClick());
        listAsuh.setAdapter(new ListAsuhAdapter(dbAdapter.getAllAsuh()));
    }

//    public class ListItemClick implements AdapterView
//            .OnItemClickListener {
//
//        @Override
//        public void onItemClick(AdapterView<?> adapter, View view,
//                                int position, long id) {
//            // TODO Auto-generated method stub
//            final Siswa siswa = (Siswa) listSiswa
//                    .getItemAtPosition(position);
//            showOptionDialog(siswa);
//        }
//    }

    public class ListItemClick implements AdapterView.OnItemClickListener{
        @Override
        public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
            //TODO Auto-generated method stub
            final Asuh asuh = (Asuh) listAsuh.getItemAtPosition(position);
            showOptionDialog(asuh);
        }
    }
    //
//    public void showOptionDialog(Siswa siswa) {
//        final Siswa mSiswa;
//        mSiswa = siswa;
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder
//                .setTitle("Test")
//                .setItems(OPTION, new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface dialog, int post) {
//                        // TODO Auto-generated method stub
//                        switch (post) {
//                            case 0:
//                                editSiswa = mSiswa;
//                                txtNama.setText(mSiswa.getNama());
//                                txtKelas.setText(mSiswa.getKelas());
//                                btnSimpan.setText("Edit");
//                                break;
//                            case 1:
//                                dbAdapter.delete(mSiswa);
//                                listSiswa.setAdapter
//                                        (new ListSiswaAdapter(dbAdapter.getAllSiswa()));
//                                break;
//                            default:
//                                break;
//                        }
//                    }
//                });
//        final Dialog d = builder.create();
//        d.show();
//    }
//
    public void showOptionDialog(Asuh asuh) {
        final Asuh mAsuh;
        mAsuh = asuh;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle("Test")
                .setItems(OPTION, new DialogInterface.OnClickListener() {
                    @Override
                public  void onClick(DialogInterface dialog, int post) {
                        //TODO Auto-generated method stub
                        switch (post) {
                            case 0:
                                editAsuh = mAsuh;
                                editText.setText(mAsuh.getNama());
                                editText2.setText(mAsuh.getTempatlahir());
                                editText3.setText(mAsuh.getTgllahir());
                                button3.setText("Edit");
                                break;
                            case 1:
                                dbAdapter.delete (mAsuh);
                                listAsuh.setAdapter(new ListAsuhAdapter(dbAdapter.getAllAsuh()));
                                break;
                            default:
                                break;
                        }
                    }
                });
        final Dialog d = builder.create();
        d.show();
    }

    //    public void save(View v) {
//        if(txtNama.getText().length() == 0 ||
//                txtKelas.getText().length() == 0) {
//            txtNama.setError("Cannot Empty");
//            txtKelas.setError("Cannot Empty");
//        } else {
//            if(btnSimpan.getText().equals("Edit")) {
//                editSiswa.setNama(txtNama.getText().toString());
//                editSiswa.setKelas(txtKelas.getText().toString());
//                dbAdapter.updateSiswa(editSiswa);
//                btnSimpan.setText("Simpan");
//            } else {
//                siswa.setNama(txtNama.getText().toString());
//                siswa.setKelas(txtKelas.getText().toString());
//                dbAdapter.save(siswa);
//            }
//            txtNama.setText("");
//            txtKelas.setText("");
//        }
//        listSiswa.setAdapter(new ListSiswaAdapter(dbAdapter
//                .getAllSiswa()));
//    }



//
//    public class ListSiswaAdapter extends BaseAdapter {
//        private  List<Siswa> listSiswa;
//
//        public ListSiswaAdapter (List<Siswa> listSiswa) {
//            this.listSiswa = listSiswa;
//        }
//
//        @Override
//        public int getCount() {
//            // TODO Auto-generated method stub
//            return this.listSiswa.size();
//        }
//
//        @Override
//        public Siswa getItem(int position) {
//            // TODO Auto-generated method stub
//            return this.listSiswa.get(position);
//        }
//
//        @Override
//        public long getItemId(int position) {
//            // TODO Auto-generated method stub
//            return position;
//        }
//
//        @Override
//        public View getView(int position, View convertView,
//                            ViewGroup parent) {
//            // TODO Auto-generated method stub
//            if(convertView == null) {
//                convertView = LayoutInflater
//                        .from(getApplicationContext())
//                        .inflate(R.layout.list_layout, parent, false);
//            }
//            final Siswa siswa = getItem(position);
//            if(siswa != null) {
//                TextView labelNama = (TextView) convertView
//                        .findViewById(R.id.labelNama);
//                labelNama.setText(siswa.getNama());
//                TextView labelKelas = (TextView) convertView
//                        .findViewById(R.id.labelKelas);
//                labelKelas.setText(siswa.getKelas());
//            }
//            return convertView;
//        }
//    }
//}


    public void save(View v) {
        if (editText.getText().length() == 0 ||
                editText2.getText().length() == 0 ||
                editText3.getText().length() == 0) {
            editText.setError("Cannot Empty");
            editText2.setError("Cannot Empty");
            editText3.setError("Cannot Empty");
        } else {
            if (button3.getText().equals("Edit")) {
                editText.setNama (editText.getText(),toString());
                editText2.setTempatlahir(editText2.getText(),toString());
                editText3.setTgllahir (editText3.getText(),toString());
                dbAdapter.updateAsuh(editAsuh);
                button3.setText("Simpan");
            } else {
                asuh.setNama(editText.getText().toString());
                asuh.setTempatlahir(editText.getText().toString());
                asuh.setTgllahir(editText.getText().toString());
                dbAdapter.save(asuh);
            }
            editText.setText("");
            editText2.setText("");
            editText3.setText("");

        }
        listAsuh.setAdapter(new ListAsuhAdapter(dbAdapter.getAllAsuh()));
    }

    public class ListAsuhAdapter extends BaseAdapter {
        private List<Asuh> listAsuh;

        public ListAsuhAdapter (List<Asuh> listAsuh) {
            this.listAsuh = listAsuh;
        }
        @Override
        public int getCont() {
            //TODO Auto-generated method stub
            return this.listAsuh.size();
        }
        @Override
        public Asuh getItem(int position) {
            //TODO auto-generated method stub
            return this.listAsuh.get(position);
        }
        @Override
        public long getItemId(int position) {
            return position;
        }
        @Override
        public View getView(int position, View convertView,ViewGroup parent) {
            // TODO Auto-generated method stub
            if(convertView == null) {
                convertView = LayoutInflater.from(getApplicationContext())
                        .inflate(R.layout.list_layout, parent, false);
            }
            final Asuh asuh = getItem(position);
            if(asuh != null) {
                TextView labelNama = (TextView) convertView
                        .findViewById(R.id.labelNama);
                labelNama.setText(asuh.getNama());

                TextView labeltmptlahir = (TextView) convertView
                        .findViewById(R.id.labeltmptlahir);
                labeltmptlahir.setText(asuh.getTempatlahir());

                TextView labeltgllahir = (TextView) convertView
                        .findViewById(R.id.labeltgllahir);
                labeltgllahir.setText(asuh.getTgllahir());
            }
            return convertView;
        }
    }
}
    }
