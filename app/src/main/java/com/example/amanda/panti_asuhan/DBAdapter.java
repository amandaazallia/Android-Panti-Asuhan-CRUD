package com.example.amanda.panti_asuhan;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.amanda.panti_asuhan.domain.Asuh;

import java.util.ArrayList;
import java.util.List;


//import java.util.ArrayList;
//import java.util.List;
//import com.dadang.biodatasiswa.domain.Siswa;
//import android.content.ContentValues;
//import android.content.Context;
//import android.database.Cursor;
//import android.database.sqlite.SQLiteDatabase;
//import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by amanda on 28/10/15.
 */


//public class DBAdapter extends SQLiteOpenHelper {
//
//    private static final String DB_NAME = "biodata";
//    private static final String TABLE_NAME = "m_siswa";
//    private static final String COL_ID = "id";
//    private static final String COL_NAME = "nama";
//    private static final String COL_KELAS = "kelas";
//    private static final String DROP_TABLE = "DROP TABLE IF EXISTS "
//            + TABLE_NAME + ";";
//    private SQLiteDatabase sqliteDatabase = null;
//
//    public DBAdapter(Context context) {
//        super(context, DB_NAME, null, 1);
//    }
//
//    @Override
//    public void onCreate(SQLiteDatabase db) {
//        createTable(db);
//    }
//
//    @Override
//    public void onUpgrade(SQLiteDatabase db, int oldVersion,
//                          int newVersion) {
//        db.execSQL(DROP_TABLE);
//    }
//
//    public void openDB() {
//        if (sqliteDatabase == null) {
//            sqliteDatabase = getWritableDatabase();
//        }
//    }
//
//    public void closeDB() {
//        if (sqliteDatabase != null) {
//            if (sqliteDatabase.isOpen()) {
//                sqliteDatabase.close();
//            }
//        }
//    }
//
//    public void createTable(SQLiteDatabase db) {
//        db.execSQL("CREATE TABLE " + TABLE_NAME + "(" + COL_ID
//                + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," + COL_NAME + " TEXT," + COL_KELAS + " TEXT);");
//    }
//
//    public void updateSiswa(Siswa siswa) {
//        sqliteDatabase = getWritableDatabase();
//        ContentValues cv = new ContentValues();
//        cv.put(COL_NAME, siswa.getNama());
//        cv.put(COL_KELAS, siswa.getKelas());
//        String whereClause = COL_ID + "==?";
//        String whereArgs[] = new String[] { siswa.getId() };
//        sqliteDatabase.update(TABLE_NAME, cv, whereClause, whereArgs);
//        sqliteDatabase.close();
//    }
public class DBAdapter extends SQLiteOpenHelper {
    private static final String DB_NAME = "crudand";
    private static final String TABLE_NAME = "m_asuh";
    private static final String COL_ID ="id";
    private static final String COL_NAME = "nama";
    private static final String COL_TEMPATLAHIR = "tempatlahir";
    private static final String COL_TGLLAHIR = "tgllahir";
    private static final String DROP_TABLE = "DROP TABLE IF EXISTS"+ TABLE_NAME + ";";
    private SQLiteDatabase sqLiteDatabase = null;


    public DBAdapter(Context context) {
        super(context, DB_NAME, null, 1);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        createTable(db);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE);
    }
    public void openDB() {
        if (sqLiteDatabase == null) {
            sqLiteDatabase = getWritableDatabase();
        }
    }
    public void closeDB() {
        if (sqLiteDatabase!=null) {
            if (sqLiteDatabase.isOpen()) {
                sqLiteDatabase.close();
            }
        }
    }
    public void createTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + "(" + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," + COL_NAME + " TEXT," + COL_TEMPATLAHIR " TEXT," + COL_TGLLAHIR " TEXT)");
    }
    public void updateAsuh(Asuh asuh) {
        sqLiteDatabase = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COL_NAME, asuh.getNama());
        cv.put(COL_TEMPATLAHIR, asuh.getTempatlahir());
        cv.put(COL_TGLLAHIR, asuh.getTgllahir());
        String whereClause = COL_ID + "==?";
        String whereArgs[] = new String[] { asuh.getId() };
        sqLiteDatabase.close();


    }
    public void save(Asuh siswa) {
        sqLiteDatabase = getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_NAME, asuh.getNama());
        contentValues.put(COL_TEMPATLAHIR, asuh.gettempatlahir());
        contentValues.put(COL_TGLLAHIR, asuh.gettgllahir());


        sqLiteDatabase.insertWithOnConflict(TABLE_NAME, null,
                contentValues, SQLiteDatabase.CONFLICT_IGNORE);

        sqLiteDatabase.close();
    }
    public void delete(Asuh asuh) {
        sqLiteDatabase = getWritableDatabase();
        String whereClause = COL_ID + "==?";
        String[] whereArgs = new String[] { String.valueOf(asuh.getId()) };
        sqLiteDatabase.delete(TABLE_NAME, whereClause, whereArgs);
        sqLiteDatabase.close();
    }
    public void deleteAll() {
        sqLiteDatabase = getWritableDatabase();
        sqLiteDatabase.delete(TABLE_NAME, null, null);
        sqLiteDatabase.close();
    }
    public List<Asuh> getAllAsuh() {
        sqLiteDatabase = getWritableDatabase();

        Cursor cursor = this.sqLiteDatabase.query(TABLE_NAME, new String[] {
                COL_ID, COL_NAME, COL_TEMPATLAHIR, COL_TGLLAHIR }, null, null, null, null, null);
        List<Asuh> siswas = new ArrayList<Asuh>();

        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                Asuh asuh = new Asuh();
                asuh.setId(cursor.getString(cursor.getColumnIndex(COL_ID)));
                asuh.setNama(cursor.getString(cursor.getColumnIndex(COL_NAME)));
                asuh.setTempatlahir(cursor.getString(cursor.getColumnIndex(COL_NAME)));
                asuh.setTgllahir(cursor.getString(cursor.getColumnIndex(COL_NAME)));

                asuhs.add(asuh);
            }
            sqLiteDatabase.close();
            return asuhs;
        } else {
            sqLiteDatabase.close();
            return new ArrayList<Asuh>();
        }
    }
}

//
//    public void save(Siswa siswa) {
//        sqliteDatabase = getWritableDatabase();
//
//        ContentValues contentValues = new ContentValues();
//        contentValues.put(COL_NAME, siswa.getNama());
//        contentValues.put(COL_KELAS, siswa.getKelas());
//
//        sqliteDatabase.insertWithOnConflict(TABLE_NAME, null,
//                contentValues, SQLiteDatabase.CONFLICT_IGNORE);
//
//        sqliteDatabase.close();
//    }
//
//    public void delete(Siswa siswa) {
//        sqliteDatabase = getWritableDatabase();
//        String whereClause = COL_ID + "==?";
//        String[] whereArgs = new String[] { String.valueOf(siswa.getId()) };
//        sqliteDatabase.delete(TABLE_NAME, whereClause, whereArgs);
//        sqliteDatabase.close();
//    }
//
//    public void deleteAll() {
//        sqliteDatabase = getWritableDatabase();
//        sqliteDatabase.delete(TABLE_NAME, null, null);
//        sqliteDatabase.close();
//    }
//
//    public List<Siswa> getAllSiswa() {
//        sqliteDatabase = getWritableDatabase();
//
//        Cursor cursor = this.sqliteDatabase.query(TABLE_NAME, new String[] {
//                COL_ID, COL_NAME, COL_KELAS }, null, null, null, null, null);
//        List<Siswa> siswas = new ArrayList<Siswa>();
//
//        if (cursor.getCount() > 0) {
//            while (cursor.moveToNext()) {
//                Siswa siswa = new Siswa();
//                siswa.setId(cursor.getString(cursor.getColumnIndex(COL_ID)));
//                siswa.setNama(cursor.getString(cursor
//                        .getColumnIndex(COL_NAME)));
//                siswa.setKelas(cursor.getString(cursor
//                        .getColumnIndex(COL_KELAS)));
//                siswas.add(siswa);
//            }
//            sqliteDatabase.close();
//            return siswas;
//        } else {
//            sqliteDatabase.close();
//            return new ArrayList<Siswa>();
//        }
//    }
//}
